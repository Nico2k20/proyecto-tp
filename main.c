#include <stdio.h>
#include <stdlib.h>



struct arbol{

    int valor;
    struct arbol *izq;
    struct arbol *der;
};


struct arbol * crearArbolPrincipal(int valor) {
    struct arbol *punterArbol;
    punterArbol = (struct arbol*) malloc (1 * sizeof(struct arbol));
    punterArbol-> valor = valor;
    punterArbol->izq = 0;
    punterArbol->der = 0;
    return punterArbol;
};


struct arbol * crearArbol(int valor,struct arbol *izq,struct arbol *der) {

    struct arbol *punterArbol;
    punterArbol = (struct arbol*) malloc (1 * sizeof(struct arbol));
    punterArbol-> valor = valor;
    punterArbol->izq = izq;
    punterArbol->der = der;


    //printf("\n valor izquierdo :  %d  valor derecho : %d",izq->valor,der->valor);

    return punterArbol;

};


void imprimirArbol (struct arbol *punteroArbol){

    if(punteroArbol==NULL) return;


    printf(" %d \n",(punteroArbol->valor));

    imprimirArbol(punteroArbol->der);
    imprimirArbol(punteroArbol->izq);
}


struct arbol * construirArbolBinario(struct arbol * arboles [],int tamanio){
    for (int j = 0; j < tamanio; j++){
       // printf("Los arboles son: izq %p, der %p\n",arboles[j]->izq,arboles[j]->der);
    }
    int valorNodo = 0;

    if((tamanio)==1){
        return arboles[0];
    }

    struct arbol *nuevosArboles [tamanio];
    int indiceArbolNuevo = 0;

    for (int i = 0; i < tamanio; i+=2){
        //printf("el indice es %d \n",i);

        printf("Por favor ingrese un valor\n");
        scanf("%d",&valorNodo);

        if((i+1)>=tamanio){
          //  printf("Soy el cero en la derecha");
            nuevosArboles[indiceArbolNuevo]  = crearArbol(valorNodo,arboles[i],0);
            indiceArbolNuevo++;
             
        }
    
      //  printf("\n No rompi en el bucle\n");
        struct arbol *izq  =  arboles[i] ;
        struct arbol *der  =  arboles[i+1];
        indiceArbolNuevo++;
        nuevosArboles[indiceArbolNuevo] = crearArbol(valorNodo,izq,der);

    }
    //printf("\n No rompi fuera del bucle  %d\n",indiceArbolNuevo);

    

   construirArbolBinario(nuevosArboles,indiceArbolNuevo);
    


}




int main(){
    int hojas = 0;
    int arboles = 0;
    int valorNodo = 0;
    int indice = 0;
    int indice1 = 0;
    int indice2 = 0;

    printf("Por favor ingrese la cantidad de hojas\n");

    scanf("%d",&hojas);

    struct arbol  *Arboleshojas [hojas];



    while(indice<hojas){
        printf("Por favor ingrese el valor de la hoja\n");
        scanf("%d",&valorNodo);
        Arboleshojas[indice] = crearArbolPrincipal(valorNodo);
        indice++;
    }

    printf("valor :  %p \n",construirArbolBinario(Arboleshojas,hojas));


    




    return 0;

}

